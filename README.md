# data_picker

Affiche un dialog permet de sélectionner un ou plusieurs éléments.

## ROADMAP

* ajout des ecran d'erreur `erreur`, `pas de connection`, ``liste vide`
* prendre en charge les multiple selection
* prendre en charge future

## Comment utiliser ?

```dart
DataPickerWidget<ClientModel>(
    title: "Selectionner un client",
    searchPlaceholder: "Entrez le nom du client",
    fecthData: Api.getClients,
    onSelected: (item) => _onChanged("client", item),
    itemBuilder: (item) => DataPickerDefaultItem(item.imageUrl, item.nom),
    buildSelectedItem: (item) => DataPickerDefaultSelectedItem(item.imageUrl, item.nom),
    match: (ClientModel item, String search) => item.nom.contains(search),
)
```

This project is a starting point for a Dart
[package](https://flutter.dev/developing-packages/),
a library module containing code that can be shared easily across
multiple Flutter or Dart projects.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
