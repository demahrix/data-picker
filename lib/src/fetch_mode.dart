
enum FetchMode {
  once,
  onlyParamsChange,
  onlySearchValueChange,
  allChanges
}
