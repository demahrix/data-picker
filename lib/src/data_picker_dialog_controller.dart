
class DataPickerDialogController<T, P> {

  late void Function(T? value) select;

  late void Function(String search) changeSearchValue;
  late void Function(P? params) changeParamsValue;

  late P? Function() getParams;

}
