import 'package:data_picker/src/data_picker_dialog_controller.dart';
import 'package:data_picker/src/fetch_mode.dart';
import 'package:flutter/material.dart';
import 'data_picker_dialog.dart';

// FIXME Click and selected element

/// Cette version (version cache) charge les donnée une seule fois sur le serveur et a chaque recherche il n'a pas besoin de retourne recherche d'autre donnée \
/// Les données recus doit etre sous forme `List<T>`.\
/// Is `readOnly` if `onSelected == null`
class DataPickerWidget<T, P> extends StatefulWidget {

  final DataPickerDialogController<T, P>? controller;

  final double height;

  /// Le titre affiché quand le dialog est ouverte
  final String title;

  final String searchPlaceholder;

  /// Si cest `true` alors on peut choisir plusieurs element avant de valider
  final bool multiple;

  /// Element selectionner initialement par defaut
  final T? initialData;

  final FetchMode fetchMode;

  final P? initialParams;

  /// typically `(data) => DataPickerDefaultItem(...)`
  final Widget Function(T item, bool selected) itemBuilder;

  /// Permet d'afficher l'element selectionné \
  /// typically `(data) => DataPickerDefaultSelectedItem(...)`
  final Widget Function(T item, VoidCallback onClose) buildSelectedItem;

  /// permet de chargment les données
  final Future<List<T>> Function(String search, P? params) fetchData;

  /// Permet de savoir si un element correspont à une recherche donnée
  /// [params] correspont au informations supplement de la recherche ex: un type ou une date donnéss
  // final bool Function<U>(T item, String search, [U options]) match;
  final bool Function(T item, String search, P? params)? match;

  /// L'element qu'on a deja selectionner
  final void Function(T?)? onSelected;

  /// Selectionne des elemenets a retenir apres avoir charge la liste des elemenets
  /// Typically `List<T>.retainWhere` test function
  final bool Function(T)? filter;

  final EdgeInsets padding;
  final Widget? placeholder;
  final Decoration? decoration;
  final Widget Function(String search, P? params)? actionsBuilder;

  const DataPickerWidget({
    super.key,
    this.controller,
    this.height = 56.0,
    required this.title,
    required this.searchPlaceholder,
    this.multiple = false,
    this.initialData,
    this.initialParams,
    required this.fetchMode,
    required this.fetchData,
    required this.itemBuilder,
    required this.onSelected,
    required this.buildSelectedItem,
    this.match,
    this.filter,
    this.placeholder,
    this.padding = const EdgeInsets.symmetric(horizontal: 12.0),
    this.decoration,
    this.actionsBuilder
  });

  @override
  State<DataPickerWidget<T, P>> createState() => _DataPickerWidgetState<T, P>();

}

class _DataPickerWidgetState<T, P> extends State<DataPickerWidget<T, P>> {

  T? _selectedData;

  @override
  void initState() {
    super.initState();
    _selectedData = widget.initialData;
    if (widget.controller != null) {
      widget.controller!.select = _selectFromController;
    }
  }

  void _onSelected(T value) {
    setState(() { _selectedData = value; });
    widget.onSelected!(value);
  }

  void _onDeleted() {
    setState(() { _selectedData = null; });
    widget.onSelected?.call(_selectedData);
  }

  void _selectFromController(T? value) {
    if (value != null)
      _onSelected(value);
    else
      _onDeleted();
  }

  void _showDialog() async {
    final value = await showDataPickerDialog<T, P>(
      context: context,
      title: widget.title,
      searchPlaceholder: widget.searchPlaceholder,
      multiple: widget.multiple,
      fetchMode: widget.fetchMode,
      initialParams: widget.initialParams,
      selectedItems: _selectedData != null ? {_selectedData as T} : const {},
      fetchData: widget.fetchData,
      itemBuilder: widget.itemBuilder,
      match: widget.match,
      actionsBuilder: widget.actionsBuilder
    );

    if (value != null)
      _onSelected(value);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onSelected != null ? _showDialog : null,
      borderRadius: BorderRadius.circular(6.0),
      child: Container(
        padding: widget.padding,
        height: widget.height,
        decoration: widget.decoration ?? BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: _selectedData == null
          ? (widget.placeholder ?? _defaultPlaceholder)
          : widget.buildSelectedItem(_selectedData as T, _onDeleted),
      ),
    );
  }

  Widget get _defaultPlaceholder {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.title,
          style: const TextStyle(
            fontWeight: FontWeight.w600
          ),
        ),
        const Icon(Icons.arrow_drop_down_outlined)
      ],
    );
  }

}
