import 'dart:io' show SocketException;
import 'dart:math' show max;
import 'package:flutter/material.dart';
import 'package:data_picker/src/data_picker_dialog_controller.dart';
import 'package:data_picker/src/fetch_mode.dart';
import 'package:data_picker/src/error_screen_widget.dart';
import 'package:data_picker/src/bloc.dart';

/// `T`: Le type des elemets dans la liste, `P`: La classe des params
/// Pour eviter les erreurs aux marquages des elements selectionnes
/// Veuillez implements des methodes `hashCode` et `operator=` sur la classe `T`
Future<T?> showDataPickerDialog<T, P>({
    required BuildContext context,
    DataPickerDialogController<T, P>? controller,
    required String title,
    required String searchPlaceholder,
    bool multiple = false,
    required FetchMode fetchMode,
    P? initialParams,
    Set<T> selectedItems = const {},
    required Widget Function(T item, bool selected) itemBuilder,
    required Future<List<T>> Function(String search, P? params) fetchData,
    bool Function(T item, String search, P? params)? match,
    Widget Function(String search, P? params)? actionsBuilder
    // Widget Function(String search, P? params)? filterWidget
  }) {
  return showDialog(
    context: context,
    builder: (_) => _DataPickerDialog(
      controller: controller,
      title: title,
      searchPlaceholder: searchPlaceholder,
      multiple: multiple,
      fetchMode: fetchMode,
      initialParams: initialParams,
      selectedItems: selectedItems,
      itemBuilder: itemBuilder,
      fetchData: fetchData,
      match: match,
      actionsBuilder: actionsBuilder,
    ),
  );
}

class _DataPickerDialog<T, P> extends StatefulWidget {

  final DataPickerDialogController<T, P>? controller;
  final String title;
  final String searchPlaceholder;
  final bool multiple;
  final FetchMode fetchMode;
  final P? initialParams;
  final Set<T> selectedItems;
  final Widget Function(T item, bool selected) itemBuilder;
  final Future<List<T>> Function(String search, P? params) fetchData;
  final bool Function(T item, String search, P? params)? match;
  final Widget Function(String search, P? params)? actionsBuilder;

  const _DataPickerDialog({
    this.controller,
    required this.title,
    required this.searchPlaceholder,
    this.multiple = false,
    required this.fetchMode,
    this.initialParams,
    this.selectedItems = const {},
    required this.itemBuilder,
    required this.fetchData,
    this.match,
    this.actionsBuilder
  }): assert(fetchMode == FetchMode.allChanges || match != null);

  @override
  __DataPickerDialogState<T, P> createState() => __DataPickerDialogState<T, P>();
}

class __DataPickerDialogState<T, P> extends State<_DataPickerDialog<T, P>> {

  late Bloc<T, P> _bloc;

  @override
  void initState() {
    super.initState();

    _bloc = Bloc(
      fetchMode: widget.fetchMode,
      multiple: widget.multiple,
      fetchData: widget.fetchData,
      match: widget.match,
      selectedItems: widget.selectedItems,
      params: widget.initialParams
    );

    if (widget.controller != null) {
      var controller = widget.controller!;
      controller.changeSearchValue = _bloc.changeSearchValue;
      controller.changeParamsValue = _bloc.changeParamsValue;
      controller.getParams = () => _bloc.params;
    }

  }

  void _onSelected(T item) {
    if (!widget.multiple) {
      Navigator.of(context).pop(item);
    } else {
      _bloc.selected(item);
    }
  }

  // FIXME unselected

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Dialog(
      child: SizedBox(
        width: max(size.width / 2.40, 520.0),
        height: size.height * 0.65,
        child: StreamBuilder<BlocData<T>>(
          initialData: const BlocData(state: BlocState.waiting),
          stream: _bloc.listen,
          builder: (context, snapshot) {

            final data = snapshot.data!;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0, bottom: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        widget.title,
                        style: const TextStyle(
                          fontSize: 19.0,
                          fontWeight: FontWeight.w600
                        ),
                      ),
                      IconButton(
                        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
                        splashRadius: 30.0,
                        padding: EdgeInsets.zero,
                        icon: Container(
                          width: 30.0,
                          height: 30.0,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: Colors.black12,
                            shape: BoxShape.circle
                          ),
                          child: const Icon(Icons.close_rounded)
                        )
                      ),
                    ],
                  ),
                ),

                _MyTextField(
                  label: widget.searchPlaceholder,
                  onChanged: _bloc.changeSearchValue,
                  actions: widget.actionsBuilder?.call(_bloc.searchValue, _bloc.params),
                ),

                const Divider(height: 1.0),

                Expanded(
                  child: _chooseWidget(data),
                )

              ],
            );
          }
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  Widget _chooseWidget(BlocData<T> data) {
    switch(data.state) {
      case BlocState.waiting:
        return const Center(child: CircularProgressIndicator());
      case BlocState.data:
        final items = data.data!;
        final selected = _bloc.selectedItems;
        return ListView.builder(
          itemCount: items.length,
          itemBuilder: (_, index) {
            var item = items[index];
            return _ListItemWidget(
              data: item,
              onTap: _onSelected,
              child: widget.itemBuilder(item, selected.contains(item)),
            );
          }
        );
      case BlocState.empty:
        // FIXME make empty element
        return const Center(child: ErrorScreenWidget('Aucun element trouve'));
      case BlocState.error:
        if (data.error is SocketException)
          return const Center(child: ErrorScreenWidget("Impossible d'accéder à internet"));
        return const Center(child: ErrorScreenWidget("Une erreur est survenue"));
    }
  }

}

class _ListItemWidget<T> extends StatelessWidget {

  final T data;
  final void Function(T) onTap;
  final Widget child;

  const _ListItemWidget({
   required this.data,
   required this.onTap,
   required this.child
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(data),
      child: child
    );
  }
}

class _MyTextField extends StatefulWidget {

  final String label;
  final void Function(String) onChanged;
  final Widget? actions;

  const _MyTextField({
    required this.label,
    required this.onChanged,
    required this.actions
  });

  @override
  __MyTextFieldState createState() => __MyTextFieldState();
}

class __MyTextFieldState extends State<_MyTextField> {

  final TextEditingController _controller = TextEditingController();

  void _clear() {
    _controller.clear();
    setState(() {});
    widget.onChanged('');
  }

  @override
  Widget build(BuildContext context) {

    final String value = _controller.value.text;

    return TextField(
      controller: _controller,
      onChanged: (newValue) {
        setState(() {});
        widget.onChanged(newValue);
      },
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        border: InputBorder.none,
        hintText: widget.label,
        prefixIcon: const Icon(Icons.search, color: Colors.black38),
        suffixIcon: _getSuffixWidget(value.isEmpty)
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _getSuffixWidget(bool isEmpty) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (!isEmpty)
          _clearIcon(),
        if (widget.actions != null)
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: widget.actions!,
          ),
        const SizedBox(width: 16.0)
      ],
    );
  }

  Widget _clearIcon() {
    return InkResponse(
      onTap: _clear,
      radius: 20.0,
      child: Container(
        width: 20.0,
        height: 20.0,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          color: Color(0x66C3C3C3),
          shape: BoxShape.circle
        ),
        child: const Icon(Icons.close, size: 12.0, color: Colors.blue)
      )
    );
  }

}
