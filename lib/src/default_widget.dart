import 'package:flutter/material.dart';
import 'avatar.dart';

class DataPickerDefaultSelectedItem extends StatelessWidget {

  final String? imageUrl;
  final String name;
  final bool showAvatar;
  final double avatarHeight; // avatar / image height
  final VoidCallback? onDelete;

  const DataPickerDefaultSelectedItem(this.name, {
    super.key, 
    this.imageUrl,
    this.showAvatar = true,
    this.avatarHeight = 16.0,
    this.onDelete
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (showAvatar)
              Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: ImageOrLettersWidget(imageUrl, name, height: avatarHeight),
              ),

            Text(name),
          ],
        ),

        if (onDelete != null)
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Material(
              color: Colors.black12,
              shape: const CircleBorder(),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: InkWell(
                  onTap: onDelete,
                  child: const Icon(Icons.close, size: 14.0),
                ),
              ),
            ),
          )
      ]
    );
  }
}

class DataPickerDefaultItem extends StatelessWidget {

  final String? imageUrl;
  final String name;
  final bool showAvatar; 
  final String? leadingText;
  final EdgeInsetsGeometry padding;
  final double avatarHeight; // avatar ou image

  const DataPickerDefaultItem(
    this.name, {
      super.key,
      this.imageUrl,
      this.showAvatar = true,
      this.leadingText,
      this.padding = const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
      this.avatarHeight = 16
    }
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (showAvatar)
            ImageOrLettersWidget(imageUrl, name, height: avatarHeight),
          if (showAvatar)
            const SizedBox(width: 12.0),
          Text(name),
          if (leadingText != null)
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  leadingText!,
                  style: const TextStyle(
                    color: Colors.grey
                  ),
                ),
              )
            )
        ]
      ),
    );
  }
}
