import 'package:flutter/material.dart';

class ErrorScreenWidget extends StatelessWidget {

  final String message;
  const ErrorScreenWidget(this.message, { super.key });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        const Icon(Icons.warning_rounded, color: Colors.red, size: 72.0),

        const SizedBox(height: 30.0,),

        Text(
          message,
          style: const TextStyle(
            color: Colors.red,
            fontWeight: FontWeight.w600,
            fontSize: 16.0
          ),
        )

      ],
    );
  }
}
