import 'dart:async';
import 'package:data_picker/src/fetch_mode.dart';

enum BlocState {
  waiting,
  data,
  error,
  empty
}

class BlocData<T> {

  final BlocState state;
  final List<T>? data;
  final Object? error;

  const BlocData({
    required this.state,
    this.data,
    this.error
  });

}

class Bloc<T, P> {

  final FetchMode fetchMode;
  final bool multiple;
  final Future<List<T>> Function(String search, P? params) fetchData;
  final bool Function(T item, String search, P? params)? match;

  final Set<T> _selectedItems;
  String _searchValue = '';
  P? _params;

  /// Only for `fetchMode.once`, `FetchMode.onlyParamsChange`, `onlySearchValueChange`
  List<T>? _items;
  BlocData<T>? _lastData;

  final StreamController<BlocData<T>> _controller = StreamController();

  Bloc({
    required this.fetchMode,
    required this.multiple,
    required this.fetchData,
    this.match,
    required Set<T> selectedItems,
    P? params
  }): _selectedItems = selectedItems, _params = params {
    _fetch();
  }

  Stream<BlocData<T>> get listen => _controller.stream;
  String get searchValue => _searchValue;
  P? get params => _params;
  Set<T> get selectedItems => _selectedItems;

  void selected(T item) {
    _selectedItems.add(item);
    if (_lastData != null)
      _controller.sink.add(_lastData!);
  }

  void unSelected(T item) {
    _selectedItems.remove(item);
    if (_lastData != null)
      _controller.sink.add(_lastData!);
  }

  // void change({ String? search, P? params }) {
  //   if (search != null)
  //     _searchValue = search;

  //   if (params != null)
  //     _params = params;

  //   if (fetchMode == FetchMode.once
  //     || (fetchMode == FetchMode.onlySearchValueChange && search == null)
  //     || (fetchMode == FetchMode.onlyParamsChange && params == null)
  //   ) {
  //     // No fetch
  //     _search();
  //   } else {
  //     _fetch();
  //   }
  // }

  void changeSearchValue(String search) {
    _searchValue = search;
    if (fetchMode == FetchMode.once || fetchMode == FetchMode.onlyParamsChange) // No fetch
      _search();
    else
      _fetch();
  }

  void changeParamsValue(P? params) {
     _params = params;
    if (fetchMode == FetchMode.once || fetchMode == FetchMode.onlySearchValueChange) // No fetch
      _search();
    else
      _fetch();
  }

  void _fetch() {
    _controller.sink.add(const BlocData(state: BlocState.waiting));
    fetchData(_searchValue, _params).then((value) {
      if (fetchMode != FetchMode.allChanges)
        _items = value;

      _lastData = value.isEmpty
        ? const BlocData(state: BlocState.empty)
        : BlocData(state: BlocState.data, data: value);
      _controller.sink.add(_lastData!);
    }, onError: (err) {
      _controller.sink.add(BlocData(state: BlocState.error, error: err));
    });
  }

  void _search() {
    assert(_items != null);

    var items = _items!;

    List<T> newList = [];

    for (int i=0, len=items.length; i<len; ++i) {
      var item = items[i];
      if (match!(item, _searchValue, _params))
        newList.add(item);
    }

    _lastData = newList.isEmpty
        ? const BlocData(state: BlocState.empty)
        : BlocData(state: BlocState.data, data: newList);
    _controller.sink.add(_lastData!);
  }

  void dispose() {
    _controller.close();
  }

}
